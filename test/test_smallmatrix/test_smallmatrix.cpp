#include "test_smallmatrix.h"
#include "gtest/gtest.h"
#include "smallmatrix/smallmatrix.h"

#define dim 4

FooTest::FooTest(){}
TEST_F(FooTest, TestBasicsNoSIMD) {
    size_t N = 1e8;
    SmallRealMatrix<double, dim, SIMDPolicy::NoSIMD> c, d;

    for(size_t i = 0; i < dim*dim; ++i) c.mat_r[i] = 0;
    for(size_t i = 0; i < dim*dim; ++i) d.mat_r[i] = 2*i;

    for(size_t i = 0; i < N; ++i) 
        c += d;
    EXPECT_EQ(c.mat_r[0], 0);
    EXPECT_EQ(c.mat_r[3], 2*3*N);
}

TEST_F(FooTest, TestBasicsSSE) {
    size_t N = 1e8;
    SmallRealMatrix<double, dim, SIMDPolicy::SSE> c, d;

    for(size_t i = 0; i < dim*dim; ++i) c.mat_r[i] = 0;
    for(size_t i = 0; i < dim*dim; ++i) d.mat_r[i] = 2*i;

    for(size_t i = 0; i < N; ++i) 
        c += d;
    EXPECT_EQ(c.mat_r[0], 0);
    EXPECT_EQ(c.mat_r[3], 2*3*N);
}

TEST_F(FooTest, TestBasicsAVX) {
    size_t N = 1e8;
    SmallRealMatrix<double, dim, SIMDPolicy::AVX> a, b;
    for(size_t i = 0; i < dim*dim; ++i) a.mat_r[i] = 0;
    for(size_t i = 0; i < dim*dim; ++i) b.mat_r[i] = 2*i;

    for(size_t i = 0; i < N; ++i) 
        a += b;
    EXPECT_EQ(a.mat_r[0], 0);
    EXPECT_EQ(a.mat_r[3], 2*3*N);
}

// TEST_F(FooTest, TestBasicsAVX512) {
//     size_t N = 1e8;
//     #define dim 2
//     SmallRealMatrix<double, dim, SIMDPolicy::AVX512> a, b;
//     for(size_t i = 0; i < dim*dim; ++i) a.mat_r[i] = 0;
//     for(size_t i = 0; i < dim*dim; ++i) b.mat_r[i] = 2*i;

//     for(size_t i = 0; i < N; ++i) 
//         a += b;
//     EXPECT_EQ(a.mat_r[0], 0);
//     EXPECT_EQ(a.mat_r[3], 2*3*N);
// }



