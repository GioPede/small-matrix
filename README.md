A fast matrix class implementation for real and complex matrices of size 2x2, 
3x3 and 4x4. Written in modern C++17 using SSE/AVX intrinsics.