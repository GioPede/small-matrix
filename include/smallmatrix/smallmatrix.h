#include <iostream>
#include <immintrin.h>

enum class SIMDPolicy {NoSIMD, SSE, AVX, AVX512};

template <typename T, int N, SIMDPolicy SIMD>
struct SmallRealMatrix {
    // Data
    T mat_r[N*N];

    constexpr SmallRealMatrix () noexcept{}
    constexpr SmallRealMatrix(const SmallRealMatrix &other) noexcept : 
            mat_r(other.mat_r) {}
    constexpr SmallRealMatrix(SmallRealMatrix &&other) noexcept : 
            mat_r(std::move(other.mat_r)) {}
    constexpr SmallRealMatrix& operator=(SmallRealMatrix other) noexcept {
        std::swap(mat_r, other.mat_r);
        return *this;
    }   

    // addition
    constexpr SmallRealMatrix& operator+=(const SmallRealMatrix &other) noexcept;
};

template <typename T, int N, SIMDPolicy SIMD>
struct SmallComplexMatrix {
    // Data
    T mat_r[N*N];
    T mat_c[N*N];

    // Initializers:
    constexpr SmallComplexMatrix () noexcept{}
    constexpr SmallComplexMatrix(const SmallComplexMatrix &other) noexcept : 
            mat_r(other.mat_r), 
            mat_c(other.mat_c)  {}
    constexpr SmallComplexMatrix(SmallComplexMatrix &&other) noexcept : 
            mat_r(std::move(other.mat_r)), 
            mat_c(std::move(other.mat_c)) {}
    constexpr SmallComplexMatrix& operator=(SmallComplexMatrix other) noexcept {
        std::swap(mat_r, other.mat_r);
        std::swap(mat_c, other.mat_c);
        return *this;
    }   
};

template <typename T, int N, SIMDPolicy SIMD>
constexpr SmallRealMatrix<T, N, SIMD>& SmallRealMatrix<T, N, SIMD>::operator+=(const SmallRealMatrix<T, N, SIMD> &other) noexcept{
    if constexpr (SIMD == SIMDPolicy::NoSIMD){
        for (size_t i = 0; i < N*N; ++i) {
            mat_r[i] += other.mat_r[i];
        }
    }
    else if constexpr (SIMD == SIMDPolicy::SSE){
        for (size_t i = 0; i < (N*N & ~0x1); i += 2 ){
            __m128d kA2   = _mm_load_pd( &mat_r[i] );
            __m128d kB2   = _mm_load_pd( &other.mat_r[i] );

            __m128d kRes = _mm_add_pd( kA2, kB2 );
            _mm_stream_pd( &mat_r[i], kRes );
        }
    }

    else if constexpr (SIMD == SIMDPolicy::AVX){
        for (size_t i = 0; i < (N*N & ~0x3); i += 4 ){
            __m256d kA4   = _mm256_load_pd( &mat_r[i] );
            __m256d kB4   = _mm256_load_pd( &other.mat_r[i] );

            __m256d kRes = _mm256_add_pd( kA4, kB4 );
            _mm256_stream_pd( &mat_r[i], kRes );
        }
    }

    else if constexpr (SIMD == SIMDPolicy::AVX512){
        for (size_t i = 0; i < (N*N & ~0x7); i += 8 ){
            __m512d kA8   = _mm512_load_pd( &mat_r[i] );
            __m512d kB8   = _mm512_load_pd( &other.mat_r[i] );

            __m512d kRes = _mm512_add_pd( kA8, kB8 );
            _mm512_stream_pd( &mat_r[i], kRes );
        }
    }
    return *this;
}